<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\FurnitureController;
use App\Http\Controllers\DominantColorController;
use App\Http\Controllers\ProjectController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();

});

Route::post('register', [AuthController::class,'register']);
Route::post('login', [AuthController::class,'login']);
Route::post('logout', [AuthController::class, 'logout']);

///Project
Route::post('create',[ProjectController::class,'create'])->middleware('auth:sanctum');
Route::post('edit/{id}',[ProjectController::class,'edit'])->middleware('auth:sanctum');
Route::delete('delete/{id}',[ProjectController::class,'delete'])->middleware('auth:sanctum');
Route::post('/add-project-image/{id}', [ProjectController::class, 'addProjectImage']);


//Furniture
Route::get('/view-furniture', [FurnitureController::class, 'view']);
Route::get('/filter-furniture', [FurnitureController::class, 'FurnitureFilter']);
Route::post('/store-furniture', [FurnitureController::class, 'store']);
//Route::post('/add-furniture/{projectId}', [FurnitureController::class, 'addFurniture']);

//Furniture
Route::post('/get-dominant-color', [DominantColorController::class, 'getDominantColor']);

