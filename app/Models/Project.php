<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'user_id',
        'color_pallet_id',
        'room_type_id',
        'room_style_id',
        'room_length',
        'room_height',
        'room_width',
//        'image'
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }
    public function furnitures()
    {
        return $this->belongsToMany(Furniture::class, 'saved_projects');
    }
}
