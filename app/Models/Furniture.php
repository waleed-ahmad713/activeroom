<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Furniture extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'category',
        'file',
    ];

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'saved_projects');
    }
}
