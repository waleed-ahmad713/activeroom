<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SavedProject extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id',
        'furniture_id',
        'position_x',
        'position_y',
        'position_z',
        'rotation_x',
        'rotation_y',
        'rotation_z'
    ];
}
