<?php

namespace App\Helper ;

class ResponseHelper
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function success($message ,$result = [],$code = 200)
    {
        $response = [
            'success' => true,
            'message' => $message,
            'data'    => $result,
            'code'    => $code
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function error($message, $code = 404, $result = [])
    {
        $response = [
            'success' => false,
            'message' => $message,
            'data' => $result
        ];

        return response()->json($response, $code);
    }

}
