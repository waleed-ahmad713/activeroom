<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function view(Request $request){

        $project = Project::query()->where('created_at', '<=', today())->paginate(4);
        return $project;

    }
    public function create(CreateProjectRequest $request){
        $data = auth()->user()->project()->create(
            $request->validated()
        );
        return response()->json(['message' => 'Project Crated successfully']);
    }
    public function edit(UpdateProjectRequest $request){
         $project = auth()->user()->project()->update($request->validated());

        return response()->json(['message' => 'Project Edited successfully']);
    }
    public function delete($id){

        $project=Project::findOrfail($id);
        $project->delete();
        return response()->json(['message' => 'Project Deleted successfully']);
    }
    public function addProjectImage(Request $request, $id)
    {
        $project = Project::find($id);
        if($request->hasFile('image')){
            $model_extension = $request->file->getClientOriginalExtension();
            $model_name = $request->file->getClientOriginalName();
        }
        $request->file->move(public_path('projects-images'), $model_name);

        $image = 'public/projects-images/' . $model_name ;
        $project->image()->create($image);

        return response()->json(['message' => 'image added successfully']);
    }
}
