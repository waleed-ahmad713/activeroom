<?php

namespace App\Http\Controllers;

use App\Models\Furniture;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use function Monolog\toArray;

class FurnitureController extends Controller
{
    public function view(){

        $furniture=Furniture::query()->where('created_at', '<=', today())->paginate(4);

        return $furniture;
    }

    public function FurnitureFilter(Request $request)
    {
        $fur = Furniture::where('category', 'LIKE', $request->category)->get();
        return $fur;
    }

    public function store(Request $request)
    {
        //dd($request->file);

        $validatedData = $request->validate([
            'name' => 'required|string',
            'category' => 'required|in:seating,table,storage,bed,dining,outdoor,entertainment,office,lighting,decorative',
            'file' => 'required|file',
        ]);

        if($request->hasFile('file')){
            $model_extension = $request->file->getClientOriginalExtension();
            $model_name = $request->file->getClientOriginalName();
            if ($model_extension != 'obj' && $model_extension != 'OBJ') {
                return response()->json(['success'=>false,'message'=>__('responses.file_format_should_be: ', ['format' => 'obj'])]);
            }
        }
        $request->file->move(public_path('furniture_models'), $model_name);
        //Storage::disk('local')->put('public/furniture_models' . '/' . $model_name, $request->file);

        $validatedData['file'] = 'public/furniture_models/' . $model_name ;
        //return $validatedData ;
        // Store the furniture data in the database
        $furniture = new Furniture();
        $furniture->name = $validatedData['name'];
        $furniture->category = $validatedData['category'];
        $furniture->file = $validatedData['file'];
        $furniture->save();

        return response()->json(['message' => 'Furniture data stored successfully']);
    }


    public function addFurniture(Request $request, $projectId)
    {
        $project = Project::find($projectId);
        $furData = $request->furData;
        $furnitures = $request->furnitureIds; // should be
        $i = 0 ;
        foreach ($furnitures as $fur)
        {
            $furnitureData = json_decode(str_replace("'", '"', $furData[$i]), true);
            $project->furnitures()->attach($fur, $furnitureData);
            $i++;

        }
    }

}
