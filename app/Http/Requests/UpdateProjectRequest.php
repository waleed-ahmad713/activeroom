<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'=>'sometimes',
            'color_pallet_id'=>'sometimes',
            'room_type_id'=>'sometimes',
            'room_style_id'=>'sometimes',
//            'image'=>'sometimes',
            'room_length'=>'sometimes',
            'room_height'=>'sometimes',
            'room_width'=>'sometimes'
        ];
    }
}
