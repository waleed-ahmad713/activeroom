<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'=>'required',
            'color_pallet_id'=>'required',
            'room_type_id'=>'required',
            'room_style_id'=>'required',
//            'image'=>'sometimes',
            'room_length'=>'required',
            'room_height'=>'required',
            'room_width'=>'required'
        ];
    }
}
