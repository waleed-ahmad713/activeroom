<?php

namespace Database\Seeders;

use App\Models\RoomType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $types = [
            [
                'name'=>'LivingRoom',

            ],
            [

                'name'=>'BedRoom',

            ],
            [

                'name'=>'DiningRoom',

            ],
            [

                'name'=>'BathRoom',

            ],
            [

                'name'=>'ChildrenRoom',

            ],
            [

                'name'=>'Kitchen',

            ],
            [

                'name'=>'Office/Study Room',

            ],
            [

                'name'=>'GuestRoom',

            ],
            [

                'name'=>'Outdoor',

            ]
        ];
        foreach ($types as $type)
        {
            RoomType::create($type);
        }
    }
}
