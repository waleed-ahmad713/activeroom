<?php

namespace Database\Seeders;

use App\Models\RoomStyle;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoomStyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $styles = [
            [
                'name'=>'Modern',

            ],
            [

                'name'=>'Traditional',

            ],
            [

                'name'=>'Scandinavian',

            ],
            [

                'name'=>'Industrial',

            ],
            [

                'name'=>'Rustic',

            ],
            [

                'name'=>'Mid-Century modern',

            ],
            [

                'name'=>'Coastal',

            ],
            [

                'name'=>'Minimalist',

            ]
        ];
        foreach ($styles as $style)
        {
            RoomStyle::create($style);
        }
    }
}
